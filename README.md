## Hugin GoPro Fusion 360 Photo Stitcher - HuGo FuPS :)

## Description
GoPro 360 can be used to record a great deal of useful 360-degree imagery, e.g. for crowdsourced street view recording (e.g. kartaview, mapillary, soon to be panoramax.fr & co). It records the images as two 180-degree images, but provides no tools to merge them on GNU/Linux (and possibly on Windows, too).

Hugin is a very capable image stitcher, but can be overwhelming to master. This project provides a GoPro Hugin template (a .pto file) tailored to this particular camera and a script with which to use it.

## Installation
There is no particular installation. You need python 3 and hugin. GNU/Linux would be good, but it's probably not hard to get it to run on something else.

## Usage
The provided preproc.py script can be used to process a directory with lots of photos in batch mode.


## Support
If you're interested in using this, let me know: I might be able to help.

## Roadmap
Would be great to improve the pto so that the mergers are nicer: undetectable merge edges, good exposure balance on the two merged photos and perhaps even camera tilting compensation.

## Contributing
Your upgrades are welcome!

## Authors and acknowledgment
TBD

## License
GPL v3

## Project status

WARNING:
If you expect to just download and run this...you're out of luck. ;) You will almost certainly have to edit the script to suit you. It would be great if you also improved the .pto template if you can, as it suffers from at least 2 issues (slightly misaligned photos during the merge and in some images, half of the merged image comes out too dark.)

But it's not too complicated and it's useful: give it a go and generate some great imagery!
