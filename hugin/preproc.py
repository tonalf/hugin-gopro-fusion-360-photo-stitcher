#!/usr/bin/python3
import glob
import os
import re
import subprocess
import sys

master_dir = sys.argv[1]
src_dir = master_dir + "/src"
# dest_dir = sys.argv[2] if len(sys.argv) == 3 else src_dir
dest_dir = master_dir + "/dest"
print("Source dir", src_dir)
print("Dest dir", dest_dir)

for b_file_name in glob.glob(src_dir + "/GB*.JPG"):
	pic_id = b_file_name.replace("GB", "").split("/")[-1]
	print(pic_id)
	f_file_name = b_file_name.replace("GB", "GF")

	if (os.path.exists(b_file_name) and os.path.exists(f_file_name)):
		# clean up if stale files around
		if (os.path.exists("GF.JPG")):
			os.remove("GF.JPG")
			os.remove("GB.JPG")
		# invert mapping in case the camera was turned backwards
		os.link(f_file_name, "GF.JPG")
		os.link(b_file_name, "GB.JPG")

#		# TODO: determine pitch
#		# TODO: adjust pitch

		subprocess.call(["hugin_executor", "-s", "-t 4", "GoProFusion360.pto"])
#		transfer EXIF metadata (may need some tweaking here)
		subprocess.call(["exiftool", "-TagsFromFile", f_file_name, "--FileName", "--Directory", "--FileSize", "--XResolution", "--YResolution", "--ResolutionUnit", "--Software", "--YCbCrSubSampling", "--ImageWidth", "--ImageHeight", "--ExifImageWidth", "--ExifImageHeight", "--ImageSize", "--Megapixels", "--CompressedBitsPerPixel", "--FieldOfView", "GB - GF.jpg"])
		merged_file = dest_dir + "/360-" + pic_id
		print(merged_file)
		if (os.path.exists(merged_file)): # remove stale files if exists
			print("Removing previously existing merged file...")
			os.remove(merged_file)
		os.link("GB - GF.jpg", merged_file)
		os.unlink("GB - GF.jpg") # effectively moved the file to merged_file

		os.remove("GB.JPG")
		os.remove("GF.JPG")
	else:
		print("Warning: missing half of image " + pic_id + "!")

message="360-degree images created"
subprocess.call(["say", message])
subprocess.call(["gvim", message])
